package com.epam.training.student_nadiia_tokareva.task1.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class PastebinHomePage extends BasePage {

    public PastebinHomePage(WebDriver driver) {
        super(driver);
    }

    private final By textArea = By.id("postform-text");
    private final By expiration = By.xpath("//span[@title='Never']");
    private final By tenMinutesOption = By.xpath("//li[text()='10 Minutes']");
    private final By title = By.id("postform-name");
    private final By submitBtn = By.xpath("//button[text()='Create New Paste']");

    public PastebinHomePage createNewPaste() throws InterruptedException {
        enterPasteContent();
        selectExpiration();
        enterPasteTitle();
        submitPaste();
        return this;
    }

    private void enterPasteContent() {
        driver.findElement(textArea).sendKeys("Hello from WebDriver");
    }

    private void selectExpiration() {
        driver.findElement(expiration).click();
        driver.findElement(tenMinutesOption).click();
    }

    private void enterPasteTitle() {

        driver.findElement(title).sendKeys("helloweb");
    }

    private void submitPaste() {
        WebElement btn = driver.findElement(submitBtn);
        waitElementsIsVisible(btn).click();
    }
}

