
package com.epam.training.student_nadiia_tokareva.task3.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CloudGoogleHomePage extends BasePage {
    public CloudGoogleHomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver,this);
    }

    @FindBy(xpath = "//div[contains(@class, 'YSM5S')]")
    private WebElement searchBtn;
    @FindBy(xpath = "//input[@aria-label='Search']")
    private WebElement inputArea;

    @FindBy(xpath = "//i[contains(@class, 'google-material-icons PETVs PETVs-OWXEXe-UbuQg')]")
    private WebElement resultOfSearch;

    @FindBy(xpath = "//a[text()='Google Cloud Pricing Calculator']")
    private WebElement refToCalcucator;


    public CloudGoogleHomePage startSearch(){
        searchBtn.click();
        inputArea.sendKeys("Google Cloud Platform Pricing Calculator");
        resultOfSearch.click();
        refToCalcucator.click();

        return this;
    }
}
