package com.epam.training.student_nadiia_tokareva.task3.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CostEstimateSummary extends BasePage {
    public CostEstimateSummary(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()='Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)']")
    protected WebElement fieldOperatingSystem;

    @FindBy(xpath = "//span[text()='4']")
    protected WebElement fieldNumberOfInstances;

    @FindBy(xpath = "//span[text()='Regular']")
    protected WebElement fieldProvisionModel;

    @FindBy(xpath = "//span[text()='n1-standard-8, vCPUs: 8, RAM: 30 GB']")
    protected WebElement fieldMachineType;

    @FindBy(xpath = "//span[text()='NVIDIA Tesla V100']")
    protected WebElement fieldGpuModel;

    @FindBy(xpath = "//span[text()='1']")
    protected WebElement fieldNumberOfGpu;

    @FindBy(xpath = "//span[text()='2x375 GB']")
    protected WebElement fieldLocalSdd;

    @FindBy(xpath = "//span[text()='Netherlands (europe-west4)']")
    protected WebElement fieldRegion;

    @FindBy(xpath = "//span[text()='1 year']")
    protected WebElement fieldDiscountOption;

}
