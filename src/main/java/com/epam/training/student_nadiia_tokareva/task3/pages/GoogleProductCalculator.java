package com.epam.training.student_nadiia_tokareva.task3.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class GoogleProductCalculator extends BasePage {
    public GoogleProductCalculator(WebDriver driver) {

        super(driver);
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//span[text()='Add to estimate']")
    private WebElement addToEstimate;

    @FindBy(xpath = "//h2[@class='honxjf'][text()='Compute Engine']")
    private WebElement addComputeEngine;

    @FindBy(id = "c11")
    private WebElement numberOfInstances;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[11]/div/div/div[2]/div/div[1]/div[3]/div/div/div/div[1]/div")
    private WebElement machineType;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[11]/div/div/div[2]/div/div[1]/div[3]/div/div/div/div[2]/ul/li[7]")
    private WebElement optionStandart8;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[21]/div/div/div[1]/div/div/span/div/button/div/span[1]")
    private WebElement addGpus;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[23]/div/div[1]/div/div/div/div[1]/div")
    private WebElement gpuModel;

    @FindBy(xpath = "//div[@data-idom-key='nvidia-tesla-p100|nvidia-tesla-p4|nvidia-tesla-v100|nvidia-tesla-t4']/ul[@aria-label='GPU Model']/li[@data-value='nvidia-tesla-v100']")
    private WebElement optionOfGpu;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[27]/div/div[1]/div/div/div/div[1]/div")
    private WebElement localSdd;

    @FindBy(xpath = "//ul[@aria-label='Local SSD']/li[@data-value='2']")
    private WebElement optinOfLocalSdd;

    @FindBy(xpath = "//*[@id=\"ucj-1\"]/div/div/div/div/div/div/div/div[1]/div/div[2]/div[3]/div[29]/div/div[1]/div/div/div/div[1]/div")
    private WebElement datacenter;

    @FindBy(xpath = "//ul[@aria-label='Region']/li[@data-value='europe-west4']")
    private WebElement netherlandsOption;

    @FindBy(xpath = "//label[text()='1 year']")
    private WebElement commitedUsage;

    @FindBy(xpath = "//label[@class='gt0C8e MyvX5d D0aEmf']")
    public WebElement totalCost;

    @FindBy(xpath = "//button[@aria-label='Open Share Estimate dialog']")
    private WebElement buttonShare;

    @FindBy(xpath = "//a[@track-name='open estimate summary']")
    private WebElement openEstimateSummary;

    public GoogleProductCalculator fillComputeEngine() {
        addToEstimate.click();
        addComputeEngine.click();
        numberOfInstances.clear();
        numberOfInstances.sendKeys("4");

        machineType.click();
        optionStandart8.click();

        addGpus.click();
        gpuModel.click();
        optionOfGpu.click();

        localSdd.click();
        optinOfLocalSdd.click();

        datacenter.click();
        netherlandsOption.click();

        commitedUsage.click();

        return this;
    }

    public GoogleProductCalculator shareSummary(){
        buttonShare.click();
        openEstimateSummary.click();

        return this;
    }
}
