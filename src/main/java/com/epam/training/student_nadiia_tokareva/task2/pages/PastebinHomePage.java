package com.epam.training.student_nadiia_tokareva.task2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class PastebinHomePage extends BasePage {
    public PastebinHomePage(WebDriver driver) {

        super(driver);
    }

    private final By textArea = By.id("postform-text");
    private final By syntaxHighlighting = By.xpath("//*[@id=\"select2-postform-format-container\"]");

    private final By expiration = By.xpath("//span[@title='Never']");
    private final By tenMinutesOption = By.xpath("//li[text()='10 Minutes']");
    private final By title = By.id("postform-name");
    private final By submitBtn = By.xpath("//button[text()='Create New Paste']");
    private final static String SYNTAX_HIGHLIGHTING_TYPE = "//li[text()='%s']";




    public PastebinHomePage enterPasteContent() {
        driver.findElement(textArea).sendKeys("git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force");
        return this;
    }

    public PastebinHomePage selectSyntaxHighlighting(String type) {
        driver.findElement(syntaxHighlighting).click();
        WebElement element = driver.findElement(By.xpath(String.format(SYNTAX_HIGHLIGHTING_TYPE, type)));
        element.click();
        return this;
    }

    public PastebinHomePage selectExpiration() {
        driver.findElement(expiration).click();
        driver.findElement(tenMinutesOption).click();
        return this;
    }

    public PastebinHomePage enterPasteTitle() {
        driver.findElement(title).sendKeys("how to gain dominance among developers");
        return this;
    }

    public PastebinHomePage submitPaste() {
        WebElement btn = driver.findElement(submitBtn);
        waitElementsIsVisible(btn).click();
        return this;
    }

    public String getTitle() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(10));
        wait.until(ExpectedConditions.titleContains("how to gain dominance among developers"));
        return driver.getTitle();
    }

    public boolean checkSyntaxHighlighted(String syntax) {
        WebElement syntaxButton = new WebDriverWait(driver, Duration.ofSeconds(10))
                .until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@href='/archive/" + syntax.toLowerCase() + "']")));
        return syntaxButton.isDisplayed();
    }

    public boolean checkText(){
        WebElement element = driver.findElement(By.xpath("//div[@class='source bash']"));
        String actualText = element.getText();
        String expectedText = "git config --global user.name  \"New Sheriff in Town\"\n" +
                "git reset $(git commit-tree HEAD^{tree} -m \"Legacy code\")\n" +
                "git push origin master --force";
        return actualText.equals(expectedText);
    }
}
