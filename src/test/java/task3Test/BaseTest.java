package task3Test;

import com.epam.training.student_nadiia_tokareva.task1.pages.PastebinHomePage;
import com.epam.training.student_nadiia_tokareva.task3.pages.BasePage;
import com.epam.training.student_nadiia_tokareva.task3.pages.CloudGoogleHomePage;
import com.epam.training.student_nadiia_tokareva.task3.pages.GoogleProductCalculator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.time.Duration;


public class BaseTest {
    WebDriver driver;
    BasePage basePage;
    CloudGoogleHomePage cloudGoogleHomePage;
    GoogleProductCalculator googleProductCalculator;

    @BeforeTest
    public void setUp(){
        driver = createDriver();
        basePage = new BasePage(driver);
        cloudGoogleHomePage = new CloudGoogleHomePage(driver);
        googleProductCalculator = new GoogleProductCalculator(driver);

    }

    public static WebDriver createDriver(){

        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        WebDriver driver = new ChromeDriver();

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        return driver;
    }

    @AfterTest
    public void closeBrowser() throws InterruptedException{
        Thread.sleep(4000);
        driver.quit();
    }

}
