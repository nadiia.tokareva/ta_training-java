package task3Test;

import com.epam.training.student_nadiia_tokareva.task3.pages.CostEstimateSummary;
import org.testng.Assert;
import org.testng.annotations.Test;

public class GoogleProductCalculatorTest extends BaseTest {

    @Test
    public void checkSearchAndFillEstimate() throws InterruptedException {
        basePage.open("https://cloud.google.com/");
        cloudGoogleHomePage.startSearch();

        basePage.open("https://cloud.google.com/products/calculator?hl=en");
        googleProductCalculator.fillComputeEngine();

        costVerification.verifyCost();

        googleProductCalculator.shareSummary();

        basePage.open("https://cloud.google.com/products/calculator/estimate-preview/e82ed9fc-d946-4e1d-97d9-9ca9fb9455aa?hl=en");
        estimateSummaryFieldsVerification.verifyValues();

    }
}



