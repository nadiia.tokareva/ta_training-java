package task3Test;

import com.epam.training.student_nadiia_tokareva.task3.pages.CostEstimateSummary;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class EstimateSummaryFieldsVerification extends CostEstimateSummary {
    public EstimateSummaryFieldsVerification(WebDriver driver) {
        super(driver);
    }
    public CostEstimateSummary verifyValues(){

        String actualOperatingSystem = fieldOperatingSystem.getText();
        Assert.assertEquals(actualOperatingSystem, "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)");

        String actualNumberOfInstances = fieldNumberOfInstances.getText();
        Assert.assertEquals(actualNumberOfInstances, "4");

        String actualProvisionModel = fieldProvisionModel.getText();
        Assert.assertEquals(actualProvisionModel, "Regular");

        String actualMachineType = fieldMachineType.getText();
        Assert.assertEquals(actualMachineType, "n1-standard-8, vCPUs: 8, RAM: 30 GB");

        String actualGpuModel = fieldGpuModel.getText();
        Assert.assertEquals(actualGpuModel, "NVIDIA Tesla V100");

        String actualNumberOfGpu = fieldNumberOfGpu.getText();
        Assert.assertEquals(actualNumberOfGpu, "1");

        String actualLocalSdd = fieldLocalSdd.getText();
        Assert.assertEquals(actualLocalSdd, "2x375 GB");

        String actualRegion = fieldRegion.getText();
        Assert.assertEquals(actualRegion, "Netherlands (europe-west4)");

        String actualDiscountOption = fieldDiscountOption.getText();
        Assert.assertEquals(actualDiscountOption, "1 year");

        return this;
    }
}
