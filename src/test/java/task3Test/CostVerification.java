package task3Test;

import com.epam.training.student_nadiia_tokareva.task3.pages.GoogleProductCalculator;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class CostVerification extends GoogleProductCalculator{
    public CostVerification(WebDriver driver) {
        super(driver);
    }
        public GoogleProductCalculator verifyCost() throws InterruptedException {

        Thread.sleep(4000);

        String actualCost = totalCost.getText();
        String expectedCost = "$5,628.90";
        Assert.assertEquals(actualCost,expectedCost);

        return this;
    }

}
