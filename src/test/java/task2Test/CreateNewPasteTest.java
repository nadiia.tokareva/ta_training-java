package task2Test;

import org.testng.Assert;
import org.testng.annotations.Test;


public class CreateNewPasteTest extends BaseTest {

    @Test
    public void checkCreateNewPaste() throws InterruptedException {
        basePage.open("https://pastebin.com/");
        pastebinHomePage.enterPasteContent();
        pastebinHomePage.selectSyntaxHighlighting("Bash");
        pastebinHomePage.selectExpiration();
        pastebinHomePage.enterPasteTitle();
        pastebinHomePage.submitPaste();

        Assert.assertEquals(pastebinHomePage.getTitle(),"how to gain dominance among developers - Pastebin.com");

        Assert.assertTrue(pastebinHomePage.checkSyntaxHighlighted("Bash"));

        Assert.assertTrue(pastebinHomePage.checkText());
    }
}
