package task1Test;


import com.epam.training.student_nadiia_tokareva.task1.pages.PastebinHomePage;
import org.testng.annotations.Test;

public class CreateNewPasteTest extends BaseTest {
    public CreateNewPasteTest() {
    }

    @Test
    public void checkCreateNewPaste() throws InterruptedException {
        basePage.open("https://pastebin.com/");
        pastebinHomePage.createNewPaste();
    }
}
